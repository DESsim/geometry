"""Provides a scripting component.
    Inputs:
        JSON: Output from Elioth geometry module
    Output:
        facades: Tree of the facades {Building;Volume;Zone}
        roofs: Tree of the roofs {Building;Volume;Zone}
        buildingIDs: List of the building IDs"""

__author__ = "Giuseppe Peronato"
__version__ = "2019.10.28"

import rhinoscriptsyntax as rs
import json
import ghpythonlib.components as gc
import math
import Rhino as rc
import ghpythonlib.treehelpers as th

import Grasshopper.Kernel.Data.GH_Path as ghpath
import Grasshopper.DataTree as datatree
import System
 
roofs = datatree[System.Object]()
facades = datatree[System.Object]()
buildingcount = 0
geometry = json.loads(JSON)
buildingIDs = []
for building in geometry:
    volumes = []
    volumesr = []
    buildingIDs.append(building["building_id"])
    buildingcount += 1
    for volume in building["volumes"]:
        zones = []
        zonesr = []
        for zone in volume["zones"]:
            floors = []
            floorsr = []
            for use in zone["uses"]:
                if use['use'] != "empty":
                    for floor in zone["floor"]:
                        ###### WALLS
                        walls = []
                        for wall in floor["walls"]:
                            if wall["adjacent"] == False:
                                    azimuth = float(wall["azimuth"])
                                    origin = rc.Geometry.Point3d(float(wall["normal_origin"][0]), float(wall["normal_origin"][1]), float(wall["altitude"]))
                                    x_size = rc.Geometry.Interval(-float(wall["width"])/2.0,float(wall["width"])/2.0)
                                    y_size = rc.Geometry.Interval(-float(wall["height"])/2.0,float(wall["height"])/2.0)
                                    normal = rc.Geometry.Vector3d(0,1,0)
                                    normal.Rotate(-math.radians(azimuth+180), rc.Geometry.Vector3d(0,0,1))
                                    plane = gc.PlaneNormal(gc.YZPlane(origin),normal)
                                    wallsrf = rc.Geometry.PlaneSurface(plane,x_size,y_size)
                                    #floors.append(wallsrf)
                                    facades.Add(wallsrf,ghpath(buildingcount,volume["volume_id"],zone["level"]))
                        ###### ROOFS
                        for roof in floor["roofs"]:
                            points = []
                            for point in roof["geometry"]:
                                points.append(rc.Geometry.Point3d(float(point[0]),float(point[1]),float(roof["altitude"])))
                            roofsrf = gc.PolyLine(points,closed=True)
                            #floorsr.append(roofsrf)
                            #roofpoints.append(rc.Geometry.Point3d(float(roof["normal_origin"][0]),float(roof["normal_origin"][1]),float(roof["altitude"])))
                            roofs.Add(roofsrf,ghpath(buildingcount,volume["volume_id"],zone["level"]))
