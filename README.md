Example use :
```
python geometry.py -v ../data/in/example/volumes.gpkg -m ../data/in/example/masks.gpkg -p ../data/in/example/programme.xlsx
```

Adapt the paths to binaries in `/python/config.json` according to your environment

R needs the following packages to be installed:
- jsonlite
- optparse
- readxl
- lwgeom
- data.table
- sf

You can use the following command in R.Studio:
```
install.packages("mypackage")
```