import sys
import subprocess
import getopt
import json
import numpy as np
from sky_view_map import add_masks
import os

def main(argv):

	# Find the path of the script
	script_path = os.path.abspath(__file__)
	module_path = os.path.dirname(os.path.dirname(script_path))
	tmp_path = os.path.join(module_path, 'data', 'tmp')

	# Read exe paths
	with open(os.path.join(module_path, 'python', 'config.json')) as json_file:
		paths = json.load(json_file)

	# Parse arguments
	volumes = ''
	masks = ''
	programme = ''

	try:
		opts, args = getopt.getopt(argv, "v:m:p:o:", ["volumes=", "masks=", "programme=", "output_path="])
	except getopt.GetoptError:
		print('geometry.py -v <volumes_gpkg_file> -m <masks_gpkg_file> -p <programme_excel_file>')
		sys.exit(2)
	for opt, arg in opts:
		if opt in ("-v", "--volumes_gpkg_file"):
			volumes = arg
		elif opt in ("-m", "--masks_gpkg_file"):
			masks = arg
		elif opt in ("-p", "--programme_excel_file"):
			programme = arg
		elif opt in ("-o", "--output_path"):
			output_path	= arg

	# Build the geometry
	subprocess.call([
		paths["R"],
		'--vanilla',
		os.path.join(module_path, 'R', 'sig_to_climelioth.R'),
		'-v', volumes,
		'-m', masks,
		'-p', programme,
		'-o', tmp_path
	])

	# Add the masks
	buildings = add_masks(tmp_path)

	# Write the results
	with open(output_path, 'w') as outfile:
		json.dump(buildings, outfile)



if __name__ == "__main__":
	main(sys.argv[1:])