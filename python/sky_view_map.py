import numpy as np
import pandas as pd
import os
import json
import shutil
from vapory import *
from matplotlib.pyplot import imread

class PovrayScene:

    def __init__(self, buildings, masks):

        objects = []
        plane = Plane([0, 1, 0], 0, Pigment('color', [1, 1, 1]))
        objects.append(plane)

        for b in buildings:
            volumes = b['volumes']
            for v in volumes:
                zones = v['zones']
                for z in zones:
                    floors = z['floor']
                    for f in floors:
                        roofs = f['roofs']
                        for r in roofs:
                            objects.append( self.roof(r['geometry'], r['altitude']) )
                            ws = self.walls(r['geometry'], r['altitude'])
                            for w in ws:
                                 objects.append( w )
                                
        for m in masks:
            if len(m['geometry']) > 0:
                objects.append( self.roof(m['geometry'], m['h_min']) )
                objects.append( self.roof(m['geometry'], m['h_max']) )
                ws = self.walls(m['geometry'], m['h_min'], m['h_max'])
                for w in ws:
                    objects.append( w )

        self.objects = objects


    def roof(self, xy, z):
        n_vertices = len(xy)
        args = [n_vertices]
        for coords in xy:
            args.append([coords[0], z, coords[1]])
        args.append(Pigment('color', [1, 1, 1]))
        p = Polygon(*args)
        return p

    def walls(self, xy, z_max, z_min=0):
        
        n_vertices = len(xy)
        
        walls = []
        
        for i in range(n_vertices):
            
            args = [5]
            
            point = xy[i]
           
            if i == n_vertices-1:
                next_point = xy[0]
            else:
                next_point = xy[i+1]
                
            args.append([point[0], z_min, point[1]])
            args.append([point[0], z_max, point[1]])
            args.append([next_point[0], z_max, next_point[1]])
            args.append([next_point[0], z_min, next_point[1]])
            args.append([point[0], z_min, point[1]])
            
            args.append(Pigment('color', [1, 1, 1]))
            
            p = Polygon(*args)
            
            walls.append(p)
                
        return walls


    def compute_mask_map(self, outfolder, positions):

        camera = Camera('fisheye', 
                        'location', ['Positions[clock][0]', "Positions[clock][2]", 'Positions[clock][1]'],
                        'look_at', ['Positions[clock][0]', 10000, 'Positions[clock][1]'],
                        'angle', 180)

        n_positions = len(positions)
        positions = json.dumps(positions).replace('[', '{').replace(']', '}')
        positions = "Positions = array[" + str(n_positions) + "][3] " + positions + ";"

        scene = Scene(camera, objects=self.objects, declare=[positions])

        pixels = scene.render(outfolder = outfolder,
                              outfile = "mask.png",
                              width=181, height=181, quality=0,
                              initial_frame = 0, final_frame = n_positions-1)

    def load_mask(self, masks_path, index, n_positions):

        n_positions = len(str(n_positions))
        index = "{0:0{1}d}".format(index, n_positions)

        pixels = imread(os.path.join(masks_path, "mask"+index+".png"))
        pixels = pixels.sum(axis=2)
        r, c = np.meshgrid(np.arange(0, pixels.shape[0]), np.arange(0, pixels.shape[0]))

        y = np.vstack((r.flatten(), c.flatten(), pixels.flatten())).T
        df = pd.DataFrame(y)
        df.columns = ['r', 'c', 'col']
        df['r'] = 180 - df['r']
        df['dx'] = (df['r'] - 90)/90
        df['dy'] = (df['c'] - 90)/90
        df['dr'] = np.sqrt(np.power(df['dx'], 2) + np.power(df['dy'], 2))
        df = df[ df['dr'] <= 1 ]
        df['psi'] = -90 - np.arctan2(df['dy'], df['dx'])*180/np.pi
        df.loc[ df['psi'] < -180, 'psi' ] = df.loc[ df['psi'] < -180, 'psi' ] + 360
        df['psi'] = -df['psi']
        df['gamma'] = (1-df['dr'])*90

        df['gamma_bin'] = np.round(df['gamma'])
        df['psi_bin'] = np.round(df['psi']/5)*5

        df.loc[ df['col'] > 0.01, 'col' ] = 1
        df = df.groupby(['psi_bin', 'gamma_bin'], as_index=False)['col'].mean()

        df = df[ df['col'] < 0.1 ]
        #df = df.groupby('psi_bin', as_index=False)['gamma_bin'].min()
        df = df.sort_values(['psi_bin', 'gamma_bin'])
        df = df.groupby('psi_bin', as_index=False)['gamma_bin'].first()

        #print(df.head())

        psi = pd.DataFrame({'psi_bin': np.arange(-180, 185, 5)})
        df = pd.merge(psi, df, on = "psi_bin", how = "left")
        df['gamma_bin'] = df['gamma_bin'].interpolate()

        df.loc[ np.isnan(df['gamma_bin']), 'gamma_bin' ] = 90
        #df.loc[ df['gamma_bin'] < 5, 'gamma_bin' ] = 5

        return df


def add_masks(tmp_path):

    with open(os.path.join(tmp_path, 'faces.json')) as file:
        buildings = json.load(file)
        file.close()
    
    with open(os.path.join(tmp_path, 'masks.json')) as file:
        masks = json.load(file)
        file.close()

    scene = PovrayScene(buildings, masks)

    # Compute masks
    positions = []

    for b in buildings:

        volumes = b['volumes']

        for v in volumes:

            zones = v['zones']
            
            for z in zones:
                floors = z['floor']
                
                for f in floors:
                    
                    walls = f['walls']
                    
                    for w in walls:
                        if w['adjacent'] is False:
                            n = np.array(w['normal_origin']) + np.array(w['normal'])
                            n = n.tolist()
                            n.append(w['altitude'])
                            positions.append(n)

                    for r in f['roofs']:
                        n = np.array(r['normal_origin'])
                        n = n.tolist()
                        n.append(r['altitude']+0.1)
                        positions.append(n)

    scene.compute_mask_map(tmp_path, positions)

    # Load the masks
    N_pos = len(positions)

    k = 0
    for b in buildings:
        # print(b['building_id'])

        volumes = b['volumes']

        for v in volumes:

            zones = v['zones']
            for z in zones:
                floors = z['floor']
                for f in floors:
                    walls = f['walls']
                    
                    for w in walls:
                        if w['adjacent'] is False:
                            mask = scene.load_mask(tmp_path, k, N_pos)
                            mask = mask.values.tolist()
                            w['mask'] = mask
                            k = k + 1
                            
                    for r in f['roofs']:
                        mask = scene.load_mask(tmp_path, k, N_pos)
                        mask = mask.values.tolist()
                        r['mask'] = mask
                        k = k + 1

    # Cleanup temp folder
    shutil.rmtree(tmp_path)
    os.makedirs(os.path.join(tmp_path))

    return buildings





